IMAGE = registry.gitlab.com/mhobson/python-alpine-pipenv
TAG = $(shell git describe --always --dirty)
TAGS = $(shell git tag --contains)

.PHONY: all build push tag_latest push_latest run

all: build tag_latest tag_all push push_latest push_all

build:
	docker build --no-cache -t $(IMAGE):$(TAG) .

push:
	docker push $(IMAGE):$(TAG)

tag_latest:
	docker tag $(IMAGE):$(TAG) $(IMAGE):latest

tag_all:
	for ATAG in $(TAGS); do \
		docker tag $(IMAGE):latest $(IMAGE):$$ATAG ; \
	done

push_latest:
	docker push $(IMAGE):latest

push_all:
	for ATAG in $(TAG) latest $(TAGS); do \
		docker push $(IMAGE):$$ATAG ; \
	done

clean:
	docker rmi $(IMAGE):$(TAG) &>/dev/null || true
	docker rmi $(IMAGE):latest &>/dev/null || true

run:
	docker run --rm -it $(IMAGE):$(TAG) sh
