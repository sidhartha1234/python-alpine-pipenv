# Based on official pipenv Dockerfile:
# https://github.com/pypa/pipenv/blob/master/Dockerfile
FROM python:3.6-alpine

# Update the distro packages and install pipenv
RUN apk update \
 && apk upgrade \
 && pip install pipenv --upgrade \
 && rm -rf /var/cache/apk/*

# Install Application into container:
RUN set -ex \
 && mkdir /app

WORKDIR /app

# Adding Pipfiles
ONBUILD COPY Pipfile Pipfile
ONBUILD COPY Pipfile.lock Pipfile.lock

# Install dependencies:
ONBUILD RUN set -ex \
         && pipenv install --deploy --system
